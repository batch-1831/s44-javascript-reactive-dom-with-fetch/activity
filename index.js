// fetch() method in JavaScript is used to send request in the server and load the received response in the webpages. The request and response is in JSoN format.

// Syntax
    // fetch('url', option)
        // url - this is the url which the request is to be made (endpoint)
        // options - array of properties that contains the HTTP method, body of request, headers.

// GET post data
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
// invoke the showPosts
.then((data) => showPosts(data))

// Add post data
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

    // Prevents the page from loading
    e.preventDefault();
    fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
            title: document.querySelector("#txt-title").value,
            body: document.querySelector("#txt-body").value,
            userId: 1
        }),
        headers: {
            "Content-Type": "application/json"
        }
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);
        alert("Successfully added!");
    })

    // resets the state of our input into blanks after submitting a new post
    document.querySelector("#txt-title").value = null;
    document.querySelector("#txt-body").value = null;
   
})

// View Posts
const showPosts = (posts) => {
    let postEntries = "";

    // We will use forEach() to display each movie inside our mock database.
    posts.forEach(post => {
        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost('${post.id}')">Edit</button>
                <button onclick="deletePost('${post.id}')">Delete</button>
            </div>
        `
    });
    // console.log(postEntries);
    document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Edit Post Button
// We will create a function that will be called in the onclick() event and will pass the value in the Update Form input box.
const editPost = (id) =>{
	// Contain the value of the title and body in a variable.
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// Pass the id, title, and body of the movie post to be updated in the Edit Post/Form.
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

    // To remove the disable property
    document.querySelector("#btn-submit-update").removeAttribute("disabled")
}

// Update post.
// This will trigger an event that will update a post upon clicking the Update button.
document.querySelector("#form-edit-post").addEventListener("submit", (e) =>{
	e.preventDefault();
	let id = document.querySelector("#txt-edit-id").value;

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: "PUT",
        body: JSON.stringify({
            id: id,
            title: document.querySelector("#txt-edit-title").value,
            body: document.querySelector("#txt-edit-body").value,
            userId: 1
        }),
        headers: {
            "Content-Type": "application/json"
        }
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);
        alert("Successfully update");

        // resets the state of our input into blanks after submitting a new post
        document.querySelector("#txt-edit-title").value = null;
        document.querySelector("#txt-edit-body").value = null;

        // add attribute in a HTML element
        document.querySelector("#btn-submit-update").setAttribute("disabled", true);
    })

})

//Activity Delete a post
const deletePost = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {method: "DELETE"})
    
    // .remove() method removes an element (or node) from the document.
    document.querySelector(`#post-${id}`).remove();
};